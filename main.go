package main

import (
	"fmt"
)

type (
	// Box our new data type
	Box interface{}

	//Stack type
	Stack struct {
		items []Box
		top   int
	}
)

// CreateStack creating a stack
func CreateStack(data ...Box) *Stack {
	var stack Stack
	stack.items = data
	stack.top = len(stack.items) - 1
	return &stack
}

// Push Push to a stack
func (s *Stack) Push(item Box) *Stack {
	s.items = append(s.items, item)
	s.top = s.top + 1
	return s
}

// Pop pop from a stack
func (s *Stack) Pop() Box {
	item := s.items[len(s.items)-1]
	s.items = s.items[:len(s.items)-1]
	s.top--
	return item
}

// Top top of stack
func (s *Stack) Top() Box {
	return s.items[s.top]
}

// IsEmpty if the stack is empty or not
func (s *Stack) IsEmpty() bool {
	return s.top == 0
}

// Print the stack
func (s *Stack) Print() {
	if s.IsEmpty() {
		fmt.Println("Stack is empty")
	} else {
		fmt.Println("Stack top is ", s.top)
		fmt.Println("Top of the stack: ", s.Top())
		for i := s.top - 1; i >= 0; i-- {
			fmt.Println("item:\t", s.items[i], " position:\t", i)
		}
	}
}

func main() {
	arr := []int{45, 23, 100, 1, 2, 55, 67}
	stack := CreateStack(34, 36)
	stack.Print()
	for _, num := range arr {
		fmt.Println("Pushing to stack: ", num)
		stack.Push(num)
	}
	stack.Print()
	fmt.Println("Poping from stack ", stack.Pop())
	fmt.Println("Poping from stack ", stack.Pop())
	fmt.Println("Poping from stack ", stack.Pop())
	stack.Print()
	fmt.Println("Pushing into stack 21")
	stack.Push(21)
	fmt.Println("Pushing into stack 86")
	stack.Push(86)
	stack.Print()

	arr2 := []string{"hello", "world", "how", "are", "you", "earth", "is"}
	stack2 := CreateStack("beautiful", "place")
	stack2.Print()
	for _, num := range arr2 {
		fmt.Println("Pushing to stack: ", num)
		stack2.Push(num)
	}
	stack2.Print()
	fmt.Println("Poping from stack ", stack2.Pop())
	fmt.Println("Poping from stack ", stack2.Pop())
	fmt.Println("Poping from stack ", stack2.Pop())
	stack2.Print()
	fmt.Println("Pushing into stack 21")
	stack2.Push("save")
	fmt.Println("Pushing into stack 86")
	stack2.Push("water")
	stack2.Print()
}
