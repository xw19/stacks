package main

import "testing"

func TestCreateStack(t *testing.T) {
	stack := CreateStack(4, 5)
	if len(stack.items) < 2 {
		t.Errorf("Stack is not getting created")
	}
}

func TestPush(t *testing.T) {
	stack := CreateStack(4, 5)
	stack.Push(6)
	if len(stack.items) < 3 {
		t.Errorf("push is not pushing")
	}
}

func TestTop(t *testing.T) {
	stack := CreateStack(4, 5)
	stack.Push(6)
	if stack.Top() != 6 {
		t.Errorf("Expected top to return %d returned %d", 6, stack.Top())
	}
}

func TestPop(t *testing.T) {
	stack := CreateStack(4, 5, 6)
	if stack.Pop() != 6 && len(stack.items) > 2 {
		t.Errorf("Expected Pop to return %d", 6)
	}
}

func TestIsEmpty(t *testing.T) {
	stack := CreateStack(4, 5)
	stack.Pop()
	stack.Pop()
	if stack.IsEmpty() == false {
		t.Errorf("Expected is empty to return true returned false")
	}
}
